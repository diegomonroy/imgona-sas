<?php get_template_part( 'part', 'banner' ); ?>
<?php
$open_div = '';
$close_div = '';
$column = '';
if ( is_page( array( 'botas-bekina', 'maquinas-picadoras-para-madera', 'aireadores-piscicultura-generadores-a-gasolina', 'bandas-transportadoras', 'mallas-para-cribado', 'maquinas-de-irrigador-de-emulsiones', 'maquinas-de-ocasion', 'maxwell-products-sellador-de-grietas-de-asfalto-modificado', 'puntas-y-bases-para-fresadoras', 'puntas-y-bases-para-tuneladoras', 'repuestos-en-fundicion', 'repuestos-plantas-asfalticas', 'repuestos-wirtgen-vogele-hamm-dynapac-volvo-caterpillar', 'trituracion', 'mandibulas-moviles', 'mandibulas-estacionarias', 'impactores-moviles', 'cono-movil', 'impactores-estacionarios', 'impactores-areneros', 'cribas-moviles', 'cribas-estacionarias', 'scalper-vibrante', 'alimentadores-grizzly', 'hidrociclones-para-arena', 'transportadores-moviles', 'plantas-electricas', 'repuestos-volquetas-dumper-volvo' ) ) ) :
	$open_div = '<div class="small-12 medium-3 columns">';
	$close_div = '</div>';
	$column = 'medium-9';
endif;
$style = '';
if ( is_page( array( 'haga-su-pedido' ) ) ) : $style = 'haga_su_pedido'; endif;
?>
<!-- Begin Content -->
	<section class="content <?php echo $style; ?>" data-wow-delay="0.5s">
		<div class="row">
			<?php echo $open_div; ?>
			<?php if ( is_page( array( 'botas-bekina', 'maquinas-picadoras-para-madera', 'aireadores-piscicultura-generadores-a-gasolina' ) ) ) : dynamic_sidebar( 'left_1' ); endif; ?>
			<?php if ( is_page( array( 'bandas-transportadoras', 'mallas-para-cribado', 'maquinas-de-irrigador-de-emulsiones', 'maquinas-de-ocasion', 'maxwell-products-sellador-de-grietas-de-asfalto-modificado', 'puntas-y-bases-para-fresadoras', 'puntas-y-bases-para-tuneladoras', 'repuestos-en-fundicion', 'repuestos-plantas-asfalticas', 'repuestos-wirtgen-vogele-hamm-dynapac-volvo-caterpillar', 'trituracion', 'mandibulas-moviles', 'mandibulas-estacionarias', 'impactores-moviles', 'cono-movil', 'impactores-estacionarios', 'impactores-areneros', 'cribas-moviles', 'cribas-estacionarias', 'scalper-vibrante', 'alimentadores-grizzly', 'hidrociclones-para-arena', 'transportadores-moviles', 'plantas-electricas', 'repuestos-volquetas-dumper-volvo' ) ) ) : dynamic_sidebar( 'left_2' ); endif; ?>
			<?php echo $close_div; ?>
			<div class="small-12 <?php echo $column; ?> columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->