<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'empresa' ) ) ) : dynamic_sidebar( 'banner_empresa' ); endif; ?>
				<?php if ( is_page( array( 'productos' ) ) ) : dynamic_sidebar( 'banner_productos' ); endif; ?>
				<?php if ( is_category( 'noticias' ) ) : dynamic_sidebar( 'banner_noticias' ); endif; ?>
				<?php if ( is_page( array( 'donde-estamos' ) ) ) : dynamic_sidebar( 'banner_donde_estamos' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->