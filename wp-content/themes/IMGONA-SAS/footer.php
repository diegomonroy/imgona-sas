		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-2' ); endif; ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php dynamic_sidebar( 'quote_here' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>