<?php get_template_part( 'part', 'banner' ); ?>
<!-- Begin Category -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="row align-center align-middle news">
						<div class="small-12 medium-5 columns text-center">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="small-12 medium-7 columns">
							<h2><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
							<p class="text-right"><a href="<?php the_permalink(); ?>" class="button"><img src="<?php echo get_site_url(); ?>/wp-content/themes/IMGONA-SAS/build/icon_more.png"> Leer más...</a></p>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Category -->